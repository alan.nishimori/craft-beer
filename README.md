# Cadastro de cervejas artesanais

Implementação do projeto de cadastro de cervejas artesanais craft-beer.

## Set up environment

Para iniciar o projeto é necessário realizar o clone deste repositório

```bash
    $ git clone https://gitlab.com/alan.nishimori/craft-beer.git
```

## Especificação do projeto

A beer house é uma empresa possui um catálogo de cervejas artesanais. Esta empresa está buscando entrar no mundo digital.
Para entrar no mundo digital a beer house dicidiu começar pelas APIs. As APIs serão utilizadas para compartilhar dados com os parceiros e também para o seu sistema web.

Pra atender a esta demanda será necessário que a você implemente as APIs do projeto beer house.

Para implementar estas APIs você dever seguir a especificação do swagger que está neste projeto.

    craftbeer
    |
    |docs
    |    |___swagger-craftbeer

