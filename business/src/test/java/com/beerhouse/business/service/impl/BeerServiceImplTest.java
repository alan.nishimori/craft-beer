package com.beerhouse.business.service.impl;

import beerhouse.dto.BeerRequest;
import com.beerhouse.business.exception.EntityNotFoundException;
import com.beerhouse.business.model.Beer;
import com.beerhouse.business.model.builder.BeerBuilder;
import com.beerhouse.business.repository.BeerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(MockitoExtension.class)
public class BeerServiceImplTest {

    @Mock
    private BeerRepository beerRepository;

    @InjectMocks
    private BeerServiceImpl beerService;

    @Test
    public void shouldSuccessfullyCreateBeer() {
        BeerRequest request = new BeerRequest();
        request.setId(0);
        request.setName(UUID.randomUUID().toString());
        request.setAlcoholContent(UUID.randomUUID().toString());
        request.setCategory(UUID.randomUUID().toString());
        request.setIngredients(UUID.randomUUID().toString());
        request.setPrice(10D);

        Mockito.when(beerRepository.insert(Mockito.any())).thenReturn(
            new BeerBuilder()
            .setId(request.getId())
            .setName(request.getName())
            .setAlcoholContent(request.getAlcoholContent())
            .setCategory(request.getCategory())
            .setIngredients(request.getIngredients())
            .setPrice(request.getPrice())
            .build()
        );

        beerService.create(request);

        Mockito.verify(beerRepository, Mockito.times(1)).insert(Mockito.any());
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenParameterIsMissing() {
        assertThrows(IllegalArgumentException.class, () -> beerService.create(new BeerRequest()));
    }

    @Test
    public void shouldSuccessfullyUpdateBeer() {
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.of(new Beer()));
        Mockito.when(beerRepository.update(Mockito.any())).thenReturn(new Beer());

        BeerRequest request = new BeerRequest();
        request.setId(0);
        request.setName(UUID.randomUUID().toString());
        request.setAlcoholContent(UUID.randomUUID().toString());
        request.setCategory(UUID.randomUUID().toString());
        request.setIngredients(UUID.randomUUID().toString());
        request.setPrice(10D);

        beerService.update(0, request);

        Mockito.verify(beerRepository, Mockito.times(1)).findById(Mockito.any());
        Mockito.verify(beerRepository, Mockito.times(1)).update(Mockito.any());
    }

    @Test
    public void shouldThrowEntityNotFoundWhenNoBeerIsFoundWhileUpdating() {
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> beerService.update(0, new BeerRequest()));
    }

    @Test
    public void shouldSuccessfullyDeleteBeerById() {
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.of(new Beer()));

        beerService.delete(0);

        Mockito.verify(beerRepository, Mockito.times(1)).findById(Mockito.any());
        Mockito.verify(beerRepository, Mockito.times(1)).deleteById(Mockito.any());
    }

    @Test
    public void shouldThrowEntityNotFoundWhenNoBeerIsFoundWhileDeleting() {
        Mockito.when(beerRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> beerService.delete(0));
    }
}
