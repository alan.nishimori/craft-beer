package com.beerhouse.business.service.impl;

import beerhouse.dto.BeerRequest;
import com.beerhouse.business.exception.EntityNotFoundException;
import com.beerhouse.business.model.Beer;
import com.beerhouse.business.model.builder.BeerBuilder;
import com.beerhouse.business.repository.BeerRepository;
import com.beerhouse.business.service.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class BeerServiceImpl implements BeerService {
    private final BeerRepository beerRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Override
    public Beer findById(Integer id) {
        Optional<Beer> beer = beerRepository.findById(id);

        return beer.orElseThrow(() -> new EntityNotFoundException("No beer found for id=" + id));
    }

    @Override
    public Collection<Beer> findAll() {
        return beerRepository.findAll();
    }

    @Override
    public Beer create(BeerRequest beerRequest) {
        Beer beer = new BeerBuilder()
            .setId(beerRequest.getId())
            .setName(beerRequest.getName())
            .setIngredients(beerRequest.getIngredients())
            .setAlcoholContent(beerRequest.getAlcoholContent())
            .setPrice(beerRequest.getPrice())
            .setCategory(beerRequest.getCategory())
            .build();

        return beerRepository.insert(beer);
    }

    @Override
    public Beer update(Integer id, BeerRequest beerRequest) {
        verifyExistence(id);

        Beer beer = new BeerBuilder()
            .setId(beerRequest.getId())
            .setName(beerRequest.getName())
            .setIngredients(beerRequest.getIngredients())
            .setAlcoholContent(beerRequest.getAlcoholContent())
            .setPrice(beerRequest.getPrice())
            .setCategory(beerRequest.getCategory())
            .build();

        return beerRepository.update(beer);
    }

    @Override
    public void delete(Integer id) {
        verifyExistence(id);

        beerRepository.deleteById(id);
    }

    private void verifyExistence(Integer id) {
        if (!beerRepository.findById(id).isPresent()) {
            throw new EntityNotFoundException("No beer found for id=" + id);
        }
    }
}
