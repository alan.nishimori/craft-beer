package com.beerhouse.business.service;

import beerhouse.dto.BeerRequest;
import com.beerhouse.business.model.Beer;

import java.util.Collection;

public interface BeerService {

    Beer findById(Integer id);

    Collection<Beer> findAll();

    Beer create(BeerRequest beerRequest);

    Beer update(Integer id, BeerRequest beerRequest);

    void delete(Integer id);
}
