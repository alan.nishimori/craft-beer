package com.beerhouse.business.model.builder;

import com.beerhouse.business.model.Beer;

import java.time.Instant;

public class BeerBuilder {
    private Integer id;
    private String name;
    private String ingredients;
    private String alcoholContent;
    private Double price;
    private String category;

    public BeerBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public BeerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public BeerBuilder setIngredients(String ingredients) {
        this.ingredients = ingredients;
        return this;
    }

    public BeerBuilder setAlcoholContent(String alcoholContent) {
        this.alcoholContent = alcoholContent;
        return this;
    }

    public BeerBuilder setPrice(Double price) {
        this.price = price;
        return this;
    }

    public BeerBuilder setCategory(String category) {
        this.category = category;
        return this;
    }

    public Beer build() {
        validateBeer();
        return new Beer(id, name, ingredients, alcoholContent, price, category, Instant.now());
    }

    private void validateBeer() {
        if (this.id == null)
            throw new IllegalArgumentException("id must not be null");

        if (this.name == null || this.name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");

        if (this.ingredients == null || this.ingredients.isEmpty())
            throw new IllegalArgumentException("ingredients must not be null nor empty");

        if (this.alcoholContent == null || this.alcoholContent.isEmpty())
            throw new IllegalArgumentException("alcoholContent must not be null nor empty");

        if (this.price == null)
            throw new IllegalArgumentException("price must not be null");

        if (this.category == null || this.category.isEmpty())
            throw new IllegalArgumentException("category must not be null nor empty");
    }
}
