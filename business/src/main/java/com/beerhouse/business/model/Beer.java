package com.beerhouse.business.model;

import java.time.Instant;
import java.util.Objects;

public class Beer {

    private Integer id;
    private String name;
    private String ingredients;
    private String alcoholContent;
    private Double price;
    private String category;
    private Instant createdAt;

    public Beer() {
    }

    public Beer(Integer id, String name, String ingredients, String alcoholContent, Double price, String category, Instant createdAt) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients;
        this.alcoholContent = alcoholContent;
        this.price = price;
        this.category = category;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getAlcoholContent() {
        return alcoholContent;
    }

    public void setAlcoholContent(String alcoholContent) {
        this.alcoholContent = alcoholContent;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Beer{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", ingredients='" + ingredients + '\'' +
            ", alcoholContent='" + alcoholContent + '\'' +
            ", price=" + price +
            ", category='" + category + '\'' +
            ", createdAt=" + createdAt +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beer beer = (Beer) o;
        return Objects.equals(id, beer.id) &&
            Objects.equals(name, beer.name) &&
            Objects.equals(ingredients, beer.ingredients) &&
            Objects.equals(alcoholContent, beer.alcoholContent) &&
            Objects.equals(price, beer.price) &&
            Objects.equals(category, beer.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, ingredients, alcoholContent, price, category);
    }
}
