package com.beerhouse.business.repository;

import com.beerhouse.business.model.Beer;

import java.util.List;
import java.util.Optional;

public interface BeerRepository {

    Beer insert(Beer beer);

    List<Beer> findAll();

    Optional<Beer> findById(Integer id);

    Beer update(Beer beer);

    void deleteById(Integer id);

    void deleteAll();
}
