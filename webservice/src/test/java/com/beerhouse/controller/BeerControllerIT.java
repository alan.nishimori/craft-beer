package com.beerhouse.controller;

import beerhouse.dto.BeerRequest;
import beerhouse.dto.ErrorResponse;
import com.beerhouse.webservice.controller.BeerController;
import com.beerhouse.base.BaseIntegrationTest;
import com.beerhouse.fixture.BeerFixture;
import com.beerhouse.business.model.Beer;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BeerControllerIT extends BaseIntegrationTest {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BeerFixture beerFixture;

    @AfterEach
    public void cleanUp() {
        beerFixture.cleanUp();
    }

    @Test
    public void shouldSuccessfullyCreateBeer() {
        ResponseEntity<Void> response = restTemplate.postForEntity(
            getTestUrl(BeerController.PATH),
            createBeerRequest(),
            Void.class
        );

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(URI.create(BeerController.PATH + "/0"), response.getHeaders().getLocation());
        beerFixture.cleanUp();
    }

    @Test
    public void shouldSuccessfullyRetrieveBeer() {
        final Beer beer = beerFixture.insert(beerFixture.create());

        final ResponseEntity<Beer> response = restTemplate.getForEntity(
            getTestUrl(BeerController.PATH + "/" + beer.getId()),
            Beer.class
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(beer, response.getBody());
        beerFixture.cleanUp();
    }

    @Test
    public void shouldSuccessfullyRetrieveAllBeers() {
        final List<Beer> beers = new ArrayList<>();
        beers.add(beerFixture.insert(beerFixture.create()));
        beers.add(beerFixture.insert(beerFixture.create()));

        final ResponseEntity<Beer[]> response = restTemplate.getForEntity(
            getTestUrl(BeerController.PATH),
            Beer[].class
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(beers.size(), Objects.requireNonNull(response.getBody()).length);
        Arrays.stream(response.getBody()).forEach(beer -> {
            assertTrue(beers.contains(beer));
        });
        beerFixture.cleanUp();
    }

    @Test
    public void shouldSuccessfullyDeleteBeerById() {
        final Beer beer = beerFixture.insert(beerFixture.create());

        final ResponseEntity<Void> response = restTemplate.exchange(
            getTestUrl(BeerController.PATH + "/" + beer.getId()),
            HttpMethod.DELETE,
            HttpEntity.EMPTY,
            Void.class
        );

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void shouldReceiveNotFoundWhenEntityIsNotFound() {
        final HttpClientErrorException.NotFound response = assertThrows(HttpClientErrorException.NotFound.class, () -> restTemplate.getForEntity(
                getTestUrl(BeerController.PATH + "/0"),
                ErrorResponse.class
            )
        );

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldReceiveNotFoundWhenDeletingAnEntityThatDoesntExist() {
        final HttpClientErrorException.NotFound response = assertThrows(HttpClientErrorException.NotFound.class, () -> restTemplate.exchange(
            getTestUrl(BeerController.PATH + "/0"),
            HttpMethod.DELETE,
            HttpEntity.EMPTY,
            ErrorResponse.class
            )
        );

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    private BeerRequest createBeerRequest() {
        BeerRequest request = new BeerRequest();
        request.setId(0);
        request.setName(UUID.randomUUID().toString());
        request.setAlcoholContent(UUID.randomUUID().toString().substring(0, 9));
        request.setCategory(UUID.randomUUID().toString());
        request.setIngredients(UUID.randomUUID().toString());
        request.setPrice(10D);

        return request;
    }
}
