package com.beerhouse.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.beerhouse")
@Import({ObjectMapperConfig.class})
public class WebServiceTestConfig {
}
