package com.beerhouse.fixture;

import com.beerhouse.business.model.Beer;
import com.beerhouse.business.model.builder.BeerBuilder;
import com.beerhouse.business.repository.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;

@Component
public class BeerFixture {
    private final BeerRepository beerRepository;

    @Autowired
    public BeerFixture(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    public Beer create() {
        return new BeerBuilder()
            .setId(new Random().nextInt(1000))
            .setName(UUID.randomUUID().toString())
            .setPrice(11.25)
            .setIngredients(UUID.randomUUID().toString())
            .setCategory(UUID.randomUUID().toString())
            .setAlcoholContent(UUID.randomUUID().toString().substring(0, 9))
            .build();
    }

    public Beer insert(Beer beer) {
        return beerRepository.insert(beer);
    }

    public void cleanUp() {
        beerRepository.deleteAll();
    }
}
