package com.beerhouse.webservice.controller;

import beerhouse.dto.BeerRequest;
import com.beerhouse.business.model.Beer;
import com.beerhouse.business.service.BeerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(
    value = BeerController.PATH,
    produces = MediaType.APPLICATION_JSON_VALUE
)
public class BeerController {
    private final BeerService beerService;

    public static final String PATH = "/beers";

    private final Logger log = LoggerFactory.getLogger(BeerController.class);

    @Autowired
    public BeerController(BeerService beerService) {
        this.beerService = beerService;
    }

    @ApiOperation(value = "Create a beer in the system")
    @ApiResponses(
        {
            @ApiResponse(
                code = 201,
                message = "Beer created"
            ),
            @ApiResponse(
                code = 400,
                message = "Incorrect input provided"
            ),
            @ApiResponse(
                code = 500,
                message = "Unknown internal error"
            )
        }
    )
    @PostMapping
    public ResponseEntity<Void> create(@Valid @RequestBody BeerRequest beer) {
        log.debug("POST /beers payload={}", beer);
        beerService.create(beer);
        log.info("Beer successfully created {}", beer);
        return ResponseEntity.created(URI.create(PATH + "/" + beer.getId())).build();
    }

    @ApiOperation(value = "Retrieve all beers")
    @ApiResponses(
        {
            @ApiResponse(
                code = 200,
                message = "Beers successfully retrieved"
            ),
            @ApiResponse(
                code = 500,
                message = "Unknown internal error"
            )
        }
    )
    @GetMapping
    public ResponseEntity<Collection<Beer>> getAll() {
        log.debug("GET /beers");
        Collection<Beer> beers = beerService.findAll();

        log.info("Retrieved {} beers", beers);
        return ResponseEntity.ok(beers);
    }

    @ApiOperation(value = "Retrieve beer by id")
    @ApiResponses(
        {
            @ApiResponse(
                code = 200,
                message = "Beer found and successfully retrieved"
            ),
            @ApiResponse(
                code = 404,
                message = "Beer not found"
            ),
            @ApiResponse(
                code = 500,
                message = "Unknown internal error"
            )
        }
    )
    @GetMapping("/{id}")
    public ResponseEntity<Beer> findById(
        @ApiParam(name = "id", value = "Integer", required = true, example = "0")
        @PathVariable(name = "id") Integer id
    ) {
        log.debug("GET /beers/{id} id={}", id);
        Beer beer = beerService.findById(id);

        log.info("Retrieved {}", beer);
        return ResponseEntity.ok(beer);
    }

    @ApiOperation(value = "Update beer by id")
    @ApiResponses(
        {
            @ApiResponse(
                code = 200,
                message = "Beer successfully updated"
            ),
            @ApiResponse(
                code = 404,
                message = "Beer not found"
            ),
            @ApiResponse(
                code = 500,
                message = "Unknown internal error"
            )
        }
    )
    @PatchMapping("/{id}")
    public ResponseEntity<Void> updateBeer(
        @ApiParam(name = "id", value = "Integer", required = true, example = "0")
        @PathVariable(name = "id") Integer id,
        @Valid @RequestBody BeerRequest beer
    ) {
        log.debug("PUT /beers/{id} id={}", id);
        beerService.update(id, beer);

        log.info("Successfully updated beer {}", beer);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Replace beer by id")
    @ApiResponses(
        {
            @ApiResponse(
                code = 200,
                message = "Beer found and successfully replaced"
            ),
            @ApiResponse(
                code = 404,
                message = "Beer not found"
            ),
            @ApiResponse(
                code = 500,
                message = "Unknown internal error"
            )
        }
    )
    @PutMapping("/{id}")
    public ResponseEntity<Void> replaceBeer(
        @ApiParam(name = "id", value = "Integer", required = true, example = "0")
        @PathVariable(name = "id") Integer id,
        @Valid @RequestBody BeerRequest beer
    ) {
        log.debug("PUT /beers/{id} id={}", id);
        beerService.update(id, beer);

        log.info("Successfully replaced beer {}", beer);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Delete beer by id")
    @ApiResponses(
        {
            @ApiResponse(
                code = 204,
                message = "Beer successfully deleted"
            ),
            @ApiResponse(
                code = 404,
                message = "Beer not found"
            ),
            @ApiResponse(
                code = 500,
                message = "Unknown internal error"
            )
        }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBeerById(
        @ApiParam(name = "id", value = "Integer", required = true, example = "0")
        @PathVariable(name = "id") Integer id
    ) {
        log.debug("DELETE /beers/{id} id={}", id);
        beerService.delete(id);

        log.info("Successfully deleted beer with id={}", id);
        return ResponseEntity.noContent().build();
    }
}
