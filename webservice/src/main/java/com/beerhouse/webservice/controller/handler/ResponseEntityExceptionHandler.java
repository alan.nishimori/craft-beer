package com.beerhouse.webservice.controller.handler;

import beerhouse.dto.ErrorResponse;
import com.beerhouse.business.exception.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ResponseEntityExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(ResponseEntityExceptionHandler.class);

    @ExceptionHandler(
        value = { EntityNotFoundException.class }
    )
    protected ResponseEntity<ErrorResponse> handleEntityNotFound(EntityNotFoundException ex) {
        log.warn(ex.getMessage());

        ErrorResponse response = new ErrorResponse();
        response.addError(ex.getMessage());
        response.setStatusCode(HttpStatus.NOT_FOUND.value());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @ExceptionHandler(
        value = { MethodArgumentNotValidException.class }
    )
    protected ResponseEntity<ErrorResponse> handleBadRequest(MethodArgumentNotValidException ex) {
        log.error("Bad request", ex);

        ErrorResponse response = new ErrorResponse();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            response.addError(error.getDefaultMessage());
        });
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(
        value = { IllegalArgumentException.class }
    )
    protected ResponseEntity<ErrorResponse> handleImproperClassCreation(IllegalArgumentException ex) {
        log.error("Improper class creation {}", ex.getMessage());

        ErrorResponse response = new ErrorResponse();
        response.addError(ex.getMessage());
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(
        value = { Exception.class }
    )
    protected ResponseEntity<ErrorResponse> handleGeneralException(Exception ex) {
        log.error("Unknown error on application", ex);

        ErrorResponse response = new ErrorResponse();
        response.addError("An internal error has occurred");
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

}
