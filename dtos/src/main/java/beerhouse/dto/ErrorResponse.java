package beerhouse.dto;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {
    private Integer statusCode;
    private final List<String> errors = new ArrayList<>();

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void addError(String error) {
        this.errors.add(error);
    }
}
