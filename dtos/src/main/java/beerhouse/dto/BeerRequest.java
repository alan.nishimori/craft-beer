package beerhouse.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class BeerRequest {
    @NotNull(message = "id is not optional")
    private Integer id;

    @NotEmpty(message = "name is not optional")
    private String name;

    @NotEmpty(message = "ingredients is not optional")
    private String ingredients;

    @NotEmpty(message = "alcoholContent is not optional")
    private String alcoholContent;

    @NotNull(message = "price is not optional")
    private Double price;

    @NotEmpty(message = "category is not optional")
    private String category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getAlcoholContent() {
        return alcoholContent;
    }

    public void setAlcoholContent(String alcoholContent) {
        this.alcoholContent = alcoholContent;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "BeerRequest{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", ingredients='" + ingredients + '\'' +
            ", alcoholContent='" + alcoholContent + '\'' +
            ", price=" + price +
            ", category='" + category + '\'' +
            '}';
    }
}
