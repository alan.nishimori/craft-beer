create table beer (
    id integer not null,
    name varchar(255) not null,
    ingredients text not null,
    alcohol_content varchar(10) not null,
    price numeric(1000, 2) not null,
    category varchar(255) not null,
    created_at timestamp not null default current_timestamp,
    constraint pk_beer primary key (id)
);
