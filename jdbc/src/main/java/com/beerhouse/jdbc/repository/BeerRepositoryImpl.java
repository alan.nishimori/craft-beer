package com.beerhouse.jdbc.repository;

import com.beerhouse.business.model.Beer;
import com.beerhouse.business.repository.BeerRepository;
import com.beerhouse.jdbc.mapper.BeerRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public BeerRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Beer insert(Beer beer) {
        String insertSQL = "insert into beer(id, name, ingredients, alcohol_content, category, price) values(?,?,?,?,?,?)";
        jdbcTemplate.update(
            insertSQL,
            beer.getId(),
            beer.getName(),
            beer.getIngredients(),
            beer.getAlcoholContent(),
            beer.getCategory(),
            beer.getPrice()
        );

        return beer;
    }

    @Override
    public List<Beer> findAll() {
        String selectAllSQL = "select * from beer";
        return jdbcTemplate.query(
            selectAllSQL,
            new BeerRowMapper()
        );
    }

    @Override
    public Optional<Beer> findById(Integer id) {
        String findById = "select * from beer where id = ?";
        List<Beer> beers = jdbcTemplate.query(
            findById,
            new Object[]{id},
            new BeerRowMapper());

        return beers.stream().findFirst();
    }

    @Override
    public Beer update(Beer beer) {
        String updateById = "update beer set (name, ingredients, alcohol_content, category, price) = (?,?,?,?,?) where id = ?";
        jdbcTemplate.update(
            updateById,
            beer.getName(),
            beer.getIngredients(),
            beer.getAlcoholContent(),
            beer.getCategory(),
            beer.getPrice() * 100,
            beer.getId()
        );
        return beer;
    }

    @Override
    public void deleteById(Integer id) {
        String deleteById = "delete from beer where id = ?";
        jdbcTemplate.update(
            deleteById,
            new Object[] {id}
        );
    }

    @Override
    public void deleteAll() {
        String deleteAll = "delete from beer";
        jdbcTemplate.update(deleteAll);
    }
}
