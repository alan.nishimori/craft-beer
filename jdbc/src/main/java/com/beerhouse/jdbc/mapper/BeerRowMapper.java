package com.beerhouse.jdbc.mapper;

import com.beerhouse.business.model.Beer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BeerRowMapper implements RowMapper<Beer> {

    @Override
    public Beer mapRow(ResultSet rs, int i) throws SQLException {
        Beer beer = new Beer();
        beer.setId(rs.getInt("id"));
        beer.setName(rs.getString("name"));
        beer.setIngredients(rs.getString("ingredients"));
        beer.setAlcoholContent(rs.getString("alcohol_content"));
        beer.setPrice(rs.getDouble("price"));
        beer.setCategory(rs.getString("category"));
        beer.setCreatedAt(rs.getTimestamp("created_at").toInstant());
        return beer;
    }
}
